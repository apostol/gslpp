// This file is part of gsl++
// (c) Apostol Faliagas, 2020

#ifndef GSLEVOL_HPP_INCLUDED
#define GSLEVOL_HPP_INCLUDED

#include <string>
#include <functional>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_odeiv2.h>
#include <gsl.hpp>

namespace gsl {
namespace odeiv2 {

/** Available algorithms */
enum {
  /**
   *  @brief Explicit embedded Runge-Kutta (2, 3) method.
   */
  RK2 = 0,
  /**
   *  @brief Explicit 4th order (classical) Runge-Kutta.
   *
   *  Error estimation is carried out by the step doubling method.
   *  For more efficient estimate of the error, use the embedded
   *  methods described below.
   */
  RK4,
  /**
   *  @brief Explicit embedded Runge-Kutta-Fehlberg (4, 5) method.
   *
   *  This method is a good general-purpose integrator.
   */
  RKF45,
  /**
   *  @brief Explicit embedded Runge-Kutta Cash-Karp (4, 5) method.
   */
  RKCK,
  /**
   *  @brief Explicit embedded Runge-Kutta Prince-Dormand (8, 9) method.
   */
  RK8PD,
  /**
   *  @brief Implicit Gaussian first order Runge-Kutta.
   *
   *  Also known as implicit Euler or backward Euler method.
   *  Error estimation is carried out by the step doubling method.
   *  This algorithm requires the Jacobian and access to the driver
   *  object via gsl_odeiv2_step_set_driver().
   */
  RK1IMP,
  /**
   *  @brief Implicit Gaussian second order Runge-Kutta.
   *
   *  Also known as implicit mid-point rule.
   *  Error estimation is carried out by the step doubling method.
   *  This stepper requires the Jacobian and access to the
   *  driver object via gsl_odeiv2_step_set_driver().
   */
  RK2IMP,
  /**
   *  @brief Implicit Gaussian 4th order Runge-Kutta.
   *
   *  Error estimation is carried out by the step doubling method.
   *  This algorithm requires the Jacobian and access to the
   *  driver object via gsl_odeiv2_step_set_driver().
   */
  RK4IMP,
  /**
   *  @brief Implicit Bulirsch-Stoer method of Bader and Deuflhard.
   *
   *  The method is generally suitable for stiff problems.
   *  Requires Jacobian.
   */
  BSIMP,
  /**
   *  @brief Variable coefficient linear multistep Adams method
   *  in Nordsieck form.
   *
   *  This stepper uses explicit Adams-Bashforth (predictor)
   *  and implicit Adams-Moulton (corrector) methods in P(EC)^m
   *  functional iteration mode. Method order varies dynamically
   *  between 1 and 12. Requires access to the driver object
   *  via gsl_odeiv2_step_set_driver().
   */
  MSADAMS,
  /**
   *  @brief Variable coefficient linear multistep BDF method.
   *
   *  It is a variable coefficient linear multistep backward
   *  differentiation formula (BDF) method in Nordsieck form.
   *  This stepper uses the explicit BDF formula as predictor
   *  and implicit BDF formula as corrector. A modified Newton
   *  iteration method is used to solve the system of non-linear
   *  equations. Method order varies dynamically between 1 and 5.
   *  The method is generally suitable for stiff problems. It
   *  requires Jacobian and access to the driver object via
   *  gsl_odeiv2_step_set_driver().
   */
  MSBDF
};

/**
 *  @class system
 *  @brief Defines a general ODE system with arbitrary parameters.
 */
class system
{
  std::function<int(double,const double[],double[])> _f;
  std::function<int(double,const double[],double*,double[])> _j;

public:
  system(size_t dim,
         const std::function<int(double, const double [], double [])> f) :
    _f(f),
    _system{_func, _jac, dim, this}
  {}
  system(size_t dim,
         const std::function<int(double,const double[],double[])> f,
         const std::function<int(double,const double[],double*,double[])> j) :
    _f(f),
    _j(j),
    _system{_func, _jac, dim, this}
  {}
  void set_jacobian(
    std::function<int(double,const double[],double*,double[])> j) {_j = j;}
  inline size_t dimension() const noexcept {return _system.dimension;}
  inline const gsl_odeiv2_system *get() const noexcept {return &_system;}
  inline gsl_odeiv2_system *get() noexcept {return &_system;}

protected:
  gsl_odeiv2_system _system;
  static
  int _func(double t, const double y[], double dydt[], void *p)
  { return ((system *)p)->_f(t, y, dydt); }
  static
  int _jac(double t, const double y[], double *dfdy, double dfdt[], void *p)
  { return ((system *)p)->_j(t, y, dfdy, dfdt); }
};

class driver;

/**
 *  @class step
 *  @brief This class contains internal parameters for a stepping function.
 */
class step
{
public:
  gsl_odeiv2_step *id;

  /**
   *  @brief Constructor
   *
   *  If you use a stepper method that requires access to a
   *  driver object, it is advisable to use a driver allocation
   *  method, which automatically allocates a stepper too.
   */
  step(int t, size_t dim)
  {
    static const gsl_odeiv2_step_type *step_types[] = {
      gsl_odeiv2_step_rk2,
      gsl_odeiv2_step_rk4,
      gsl_odeiv2_step_rkf45,
      gsl_odeiv2_step_rkck,
      gsl_odeiv2_step_rk8pd,
      gsl_odeiv2_step_rk1imp,
      gsl_odeiv2_step_rk2imp,
      gsl_odeiv2_step_rk4imp,
      gsl_odeiv2_step_bsimp,
      gsl_odeiv2_step_msadams,
      gsl_odeiv2_step_msbdf
    };
    id = gsl_odeiv2_step_alloc(step_types[t], dim);
  }
  /** Destructor */
  ~step() { gsl_odeiv2_step_free(id); }
  /**
   *  @brief Resets the stepping function.
   *
   *  It should be used whenever the next use of the stepping
   *  function will not be a continuation of a previous step.
   */
  int reset() { return gsl_odeiv2_step_reset(id); }
  /** Returns the name of the stepping function. */
  const std::string name() const {return gsl_odeiv2_step_name(id);}
  /**
   *  @brief Returns the order of the stepping function in the previous step.
   *
   *  The order can vary if the stepping function itself is adaptive.
   */
  unsigned int order() const {return gsl_odeiv2_step_order(id);}
  /** @brief Set a driver object d for this stepper
   *
   *  This method sets the driver \c d. This is required for some
   *  steppers in order to get the desired error level for the
   *  internal iteration of the stepper.
   *  Allocation of a driver object calls this function automatically.
   */
  inline int set_driver(const driver& d);
  /**
   *  @brief Apply stepping function.
   *
   *  This function applies the stepping function to the system
   *  of equations \c sys, using the step \c h to advance the
   *  system from time \c t and state \c y to time \c t + \c h.
   *  The new state of the system is stored in \c y on output,
   *  and an estimate of the absolute error for each component
   *  is stored in \c yerr. When the argument \c dydt_in is not
   *  null it contains the derivatives at time \c t on input. This
   *  is optional as the derivatives are computed internally if
   *  they are not provided, but when it is used it allows the
   *  reuse of existing derivative information. On output the new
   *  derivatives of the system at time \c t + \c h will be stored
   *  in \c dydt_out if it is not null.
   *
   * The stepping function returns \c GSL_FAILURE if it is unable
   *  to compute the requested step. Also, if the user-supplied
   *  functions defined in \c sys return a status other than
   *  \c GSL_SUCCESS the step will be aborted. In this case, the
   *  elements of \c y are restored to their pre-step values and
   *  the error code from the user-supplied function is returned.
   *  Failure may be due to a singularity in the system or a too
   *  large step \c h. In this case the step should be attempted
   *  again with a smaller size, e.g. \c h / 2.
   *
   *  If the driver object is not appropriately set via
   *  \c gsl_odeiv2_step_set_driver() for steppers that need it,
   *  the stepping function returns \c GSL_EFAULT. If the
   *  user-supplied functions in \c sys return \c GSL_EBADFUNC
   *  the function returns immediately with the same return code.
   *  In this case the user must call \c gsl_odeiv2_step_reset()
   *  before calling this function again.
  */
  int apply(double t, double h, double y[], double yerr[],
            const double dydt_in[], double dydt_out[], const system& sys)
  { return gsl_odeiv2_step_apply(id, t, h, y,yerr,dydt_in,dydt_out,sys.get()); }
  /** Get step type */
  static const gsl_odeiv2_step_type *type(int t)
  {
    static const gsl_odeiv2_step_type *step_types[] = {
      gsl_odeiv2_step_rk2,
      gsl_odeiv2_step_rk4,
      gsl_odeiv2_step_rkf45,
      gsl_odeiv2_step_rkck,
      gsl_odeiv2_step_rk8pd,
      gsl_odeiv2_step_rk1imp,
      gsl_odeiv2_step_rk2imp,
      gsl_odeiv2_step_rk4imp,
      gsl_odeiv2_step_bsimp,
      gsl_odeiv2_step_msadams,
      gsl_odeiv2_step_msbdf
    };
    return step_types[t];
  }
};

/**
 *  @class control
 *  @brief Adaptive step size control
 *
 *  Control examines the proposed change of the solution produced
 *  by a stepping object and attempts to determine the optimal
 *  step size for a user-specified level of error.
 */
class control
{
public:
  gsl_odeiv2_control *id;

  /**
   *  @brief Constructor for standard control object.
   *
   *  The standard control object uses a 4-parameter heuristic
   *  method based on absolute and relative errors \c eps_abs
   *  and \c eps_rel and scaling factors \c a_y and \c a_dydt
   *  for the system state \c y(t) and derivatives \c y'(t)
   *  respectively.
   *
   *  The step size adjustment procedure for this method begins
   *  by computing the desired error level \c D_i for each component,
   *
   *  \f[
   *    D_i = \epsilon_{abs}+\epsilon_{rel}*(a_{y}|y_i|+a_{dydt}h|y\prime_i|)
   *  \f]
   *
   *  and comparing it with the observed error \f$E_i = |yerr_i|\f$.
   *  If the observed error \c E exceeds the desired error level
   *  \c D by more than 10% for any component then the method
   *  reduces the step size by an appropriate factor,
   *
   *  \f[
   *    h_{new} = h_{old} * S * (E/D)^{-1/q}
   *  \f]
   *
   *  where \c q is the consistency order of the method (e.g.
   *  q=4 for 4(5) embedded RK), and \c S is a safety factor of
   *  0.9. The ratio \c E/D is taken to be the maximum of the
   *  ratios \f$E_i/D_i\f$.
   *
   *  If the observed error \c E is less than 50% of the desired
   *  error level \c D for the maximum ratio \f$E_i/D_i\f$ then
   *  the algorithm takes the opportunity to increase the step
   *  size to bring the error in line with the desired level,
   *
   *  \f[
   *    h_{new} = h_{old} * S * (E/D)^{-1/(q+1)}
   *  \f]
   *
   *  This encompasses all the standard error scaling methods.
   *  To avoid uncontrolled changes in the stepsize, the overall
   *  scaling factor is limited to the range 1/5 to 5.
   */
  control(double eps_abs, double eps_rel, double a_y, double a_dydt) :
    id(gsl_odeiv2_control_standard_new(eps_abs, eps_rel, a_y, a_dydt)) {}
  /**
   *  @brief Construct object controling solution error within set limits.
   *
   *  The constructed object controls the step size so that in
   *  each step the local absolute error of the solution
   *  \f$y_i(t)\f$ is within \c eps_abs and the relative error
   *  within \c eps_rel. This is equivalent to the standard
   *  control object with \c a_y = 1 and \c a_dydt = 0.
   */
  control(double eps_abs, double eps_rel) :
    id(gsl_odeiv2_control_y_new(eps_abs, eps_rel)) {}
  /**
   *  @brief Construct object controling time derivative of
   *         solution error within set limits.
   *
   *  The constructed object controls the step size so that in
   *  each step the local absolute error of the time derivative
   *  of the solution \f$y^{\prime}_i(t)\f$ is within \c eps_abs
   *  and the relative error within \c eps_rel. This is
   *  equivalent to the standard control object with \c a_y = 0
   *  and \c a_dydt = 1.
   */
  control(double eps_abs, double eps_rel, bool) :
    id(gsl_odeiv2_control_yp_new(eps_abs, eps_rel)) {}
  /**
   *  @brief Constructor for standard control object with
   *         individual scaling parameters.
   *
   *  The constructe object is similar to the standard control
   *  object with the difference that each individual component
   *  is scaled according to the components of the array
   *  \c scale_abs. \f$D_i\f$ for this control object is
   *
   *  \f[
   *    D_i=\epsilon_{abs}s_i+\epsilon_{rel}*(a_{y}|y_i|+a_{dydt}h|y\prime_i|)
   *  \f]
   *
   *  where \f$s_i\f$ is the \f$i\f$-th component of the array
   *  \c scale_abs.
   *
   *  This is the error control method used in the Matlab ODE suite.
   */
  control(double eps_abs, double eps_rel, double a_y, double a_dydt,
          const double scale_abs[], size_t dim) :
    id(gsl_odeiv2_control_scaled_new(eps_abs,eps_rel,a_y,a_dydt,scale_abs,dim)){}
  /**
   *  @brief Constructor for custom control objects.
   *
   *  This function is used for defining new types of control
   *  functions. For most purposes the standard control functions
   *  described above should be sufficient.
   */
  control(const gsl_odeiv2_control_type *T) :
    id(gsl_odeiv2_control_alloc(T)) {}
  /** Destructor */
  ~control() {gsl_odeiv2_control_free(id);}
  /**
   *  @brief Initialize this control function.
   *
   *  @param eps_abs absolute error
   *  @param eps_rel relative error,
   *  @param a_y scaling factor for \c y
   *  @param a_dydt scaling factor for derivatives
   */
  int init(double eps_abs, double eps_rel, double a_y, double a_dydt)
  { return gsl_odeiv2_control_init(id, eps_abs, eps_rel, a_y, a_dydt); }
  /**
   *  @brief Adjust step size according to current values of \c y, \c yerr
   *         and \c dydt.
   *
   *  The stepping object \c step is also needed to determine
   *  the order of the method. If the error in the y-values
   *  \c yerr is found to be too large then the step size \c h
   *  is reduced and the function returns \c GSL_ODEIV_HADJ_DEC.
   *  If the error is sufficiently small then \c h may be increased
   *  and \c GSL_ODEIV_HADJ_INC is returned. The function returns
   *  \c GSL_ODEIV_HADJ_NIL if the step size is unchanged. The
   *  goal of the function is to estimate the largest step size
   *  which satisfies the user-specified accuracy requirements
   *  for the current point.
   */
  int hadjust(step& s, const double y[], const double yerr[],
              const double dydt[], double *h)
  { return gsl_odeiv2_control_hadjust(id, s.id, y, yerr, dydt, h); }
  /** Get name of the control function. */
  const std::string name() {return gsl_odeiv2_control_name(id);}
  /**
   *  @brief Calculate the desired error level of the i-th component
   *         of \c errlev.
   *
   *  @param y value of i-th component
   *  @param dydt  value of time derivative of i-th component
   *  @param h step size
   *  @param i component
   *  @param errlev error level
   */
  int errlevel(const double y, const double dydt, const double h,
               const size_t i, double *errlev)
  { return gsl_odeiv2_control_errlevel(id, y, dydt, h, i, errlev); }
  /** Set pointer of driver object. */
  inline int set_driver(const driver& d);
};

/**
 *  @class evolve
 *  @brief Encapsulation of gsl_odeiv2_evolve
 *
 *  Evolution combines a \c step with a \c control to advance the
 *  solution one step forward using an acceptable step size.
 */
class evolve
{
public:
  /** Contains parameters controlling the evolution. */
  gsl_odeiv2_evolve *id;
  /** Constructor */
  explicit evolve(size_t dim) : id(gsl_odeiv2_evolve_alloc(dim)) {}
  /** Destructor */
  ~evolve() {gsl_odeiv2_evolve_free(id);}
  /**
   *  @brief Advances the evolution system by one step.
   *
   *  The new time and position are stored in \c t and \c y on
   *  output. The initial step size taken is \c h. The control
   *  object \c con is applied to check whether the local error
   *  estimated by taking a step with size \c h exceeds the
   *  error tolerance. If the error is too high, the step is
   *  taken anew with a smaller step size. This process is
   *  continued until an acceptable step size is found. An
   *  estimate of the local error can be obtained from the
   *  components of the array \c e->yerr[].
   *
   *  If the user-supplied functions of the system \c sys return
   *  \c GSL_EBADFUNC, the function returns immediately with the
   *  same return code. In this case the user must call step::reset()
   *  and evolve::reset() before calling this function again.
   *  Otherwise, if the user-supplied functions defined in the
   *  system or \c step return a status other than \c GSL_SUCCESS,
   *  the step is taken again with a smaller step size. If the
   *  step size decreases below machine precision, \c GSL_FAILURE
   *  is returned if the user functions returned \c GSL_SUCCESS.
   *  Otherwise the value returned by the user's function is
   *  returned. If no acceptable step can be taken, \c t and \c y
   *  are restored to their original values and \c h contains the
   *  last tried step size.
   *
   *  If the step is successful the function returns a suggested
   *  step size for the next step \c h. The maximum time \c t1 is
   *  guaranteed not to be exceeded by the time step. In the final
   *  time step the value of \c t is set to \c t1.
   */
  int apply(control& con, step& s, const system& sys, double& t, double t1,
                                                      double& h, double y[])
  { return gsl_odeiv2_evolve_apply(id, con.id, s.id, sys.get(), &t, t1, &h,y); }
  /**
   *  @brief Advance the ODE-system by a specified step size.
   *
   *  If the local error estimated by the stepping object exceeds
   *  the set error level, the step is not taken and the function
   *  returns \c GSL_FAILURE. Otherwise the value returned by the
   *  user's function is returned.
   */
  int apply_fixed_step(control& con, step& s, const system& sys, double& t,
                       const double h, double y[])
  { return gsl_odeiv2_evolve_apply_fixed_step(id,con.id,s.id,sys.get(),&t,h,y); }
  /**
   *  @brief Reset the evolution function.
   *
   *  It should be used whenever the next use of \c this will not
   *  be the continuation of a previous step.
   */
  inline int reset() {return gsl_odeiv2_evolve_reset(id);}
  /**
   *  @brief Set driver object.
   *
   *  If a system has discontinuous changes in the derivatives at
   *  known points, it is advisable to evolve the system at each
   *  discontinuity in sequence. For example, if a step change in
   *  external driving forces occurs at times \f$t_a\f$, \f$t_b\f$
   *  and \f$t_c\f$ then evolution must be carried out over the
   *  ranges \f$(t_0,t_a)\f$, \f$(t_a,t_b)\f$, \f$(t_b,t_c)\f$
   *  and \f$(t_c,t_1)\f$ separately, and not directly over the
   *  interval \f$(t_0,t_1)\f$.
   */
  inline int set_driver(const driver& d);
};

/**
 *  @class driver
 *  @brief Wrapper for the evolution, control and step objects combined.
 *
 *  It is a high level wrapper combining evolution, control and
 *  step objects for convenience.
 */
class driver
{
public:
  gsl_odeiv2_driver *id;

  /** Constructor */
  driver(const system& sys, int t, const double hstart, const double epsabs,
                                                        const double epsrel) :
    id(gsl_odeiv2_driver_alloc_y_new(sys.get(), step::type(t), hstart, epsabs,
                                     epsrel)) {}
  /** Constructor */
  driver(const system& sys, int t, const double hstart, const double epsabs,
                                   const double epsrel, bool) :
    id(gsl_odeiv2_driver_alloc_yp_new(sys.get(), step::type(t), hstart, epsabs,
                                      epsrel)) {}
  /** Constructor */
  driver(const system& sys, int t, const double hstart, const double epsabs,
         const double epsrel, const double a_y, const double a_dydt) :
    id(gsl_odeiv2_driver_alloc_standard_new(sys.get(), step::type(t), hstart,
                                            epsabs, epsrel, a_y, a_dydt)) {}
  /** Constructor */
  driver(const system& sys, int t, const double hstart, const double epsabs,
         const double epsrel, const double a_y, const double a_dydt,
         const double scale_abs[]) :
    id(gsl_odeiv2_driver_alloc_scaled_new(sys.get(), step::type(t), hstart,
                                          epsabs, epsrel, a_y, a_dydt,
                                          scale_abs)) {}
  /** Destructor */
  ~driver() {gsl_odeiv2_driver_free(id);}
  /**
   *  @brief Set minimum for step size.
   *
   *  Default value is 0.
   */
  int set_hmin(const double hmin)
  { return gsl_odeiv2_driver_set_hmin(id, hmin); }
  /**
   *  @brief Set maximum for step size.
   *
   *  Default value is \c GSL_DBL_MAX.
   */
  int set_hmax(const double hmax)
  { return gsl_odeiv2_driver_set_hmax(id, hmax); }
  /**
   *  @brief Set maximum for number of steps.
   *
   *  Default value is 0 and it sets no limit for the number of steps.
   */
  int set_nmax(const unsigned long int nmax)
  { return gsl_odeiv2_driver_set_nmax(id, nmax); }

  /**
   * @brief Evolve the driver system through a time interval.
   *
   * This method evolves the driver system from \c t to \c t1.
   *  Initially, \c y should contain the values of dependent
   *  variables at \c t. If the function is unable to complete
   *  the calculation, an error code from gsl_odeiv2_evolve_apply()
   *  is returned, and \c t and \c y contain the values of the
   *  last successful step.
   *
   *  If the maximum number of steps is reached, \c GSL_EMAXITER
   *  is returned. If the step size drops below the minimum value,
   *  the function returns with \c GSL_ENOPROG. If the user-supplied
   *  functions defined in the system return \c GSL_EBADFUNC, the
   *  function returns immediately with the same return code. In
   *  this case the user must call driver::reset() before calling
   *  this function again.
   */
  int apply(double &t, const double t1, double y[])
  { return gsl_odeiv2_driver_apply(id, &t, t1, y); }
  /**
   *  @brief Evolve the driver system by \c n steps size \c h.
   *
   *  If the function is unable to complete the calculation, an
   *  error code from gsl_odeiv2_evolve_apply_fixed_step() is
   *  returned, and \c t and \c y contain the values of the last
   *  successful step.
   */
  int apply_fixed_step(double &t, const double h, const unsigned long int n,
                       double y[])
  { return gsl_odeiv2_driver_apply_fixed_step(id, &t, h, n, y); }
  /** Reset the evolution and step objects. */
  int reset()
  { return gsl_odeiv2_driver_reset(id); }
  /**
   *  @brief Reset the evolution and step objects and the initial step size.
   *
   *  This function can be used e.g. to change the direction of integration.
   *  @param hstart The new initial step size
   */
  int reset_hstart(const double hstart)
  { return gsl_odeiv2_driver_reset_hstart(id, hstart); }
};

inline int step::set_driver(const driver& d)
{ return gsl_odeiv2_step_set_driver(id, d.id); }

inline int control::set_driver(const driver& d)
{ return gsl_odeiv2_control_set_driver(id, d.id); }

inline int evolve::set_driver(const driver& d)
{ return gsl_odeiv2_evolve_set_driver(id, d.id); }

/**
 *  @class evolution
 *  @brief Higher level evolution class
 */
class evolution
{
  using func_t   = std::function<void(double,
                                      gsl::vector<double>&,
                                      const gsl::vector<double>&)>;
  using jac_t    = std::function<void(double,
                                      gsl::vector<double>&,
                                      const gsl::vector<double>&,
                                      const gsl::vector<double>&)>;
  using stepcb_t = std::function<bool(int, double,
                                      const gsl::vector<double>&,
                                      const gsl::vector<double>&)>;

  bool time_dependent_;
  func_t func_;
  jac_t  jac_;
  std::function<void(double, gsl::vector<double>&)> renormalize_{nullptr};

  double ht_;
  double hs_;
  gsl::odeiv2::system  sys_;
  gsl::odeiv2::driver  drv_;
  gsl::odeiv2::step    step_;
  gsl::odeiv2::control ctrl_;
  gsl::odeiv2::evolve  evol_;
  double t_start_;
  double t_end_;

public:

  evolution(size_t N, int alg, double start, double end,
      double epsa, double hs,
      func_t func,
      double ht = 1e-3, double epsr = 0) :
    time_dependent_(false),
    func_(func),
    ht_(ht),
    hs_(hs),
    sys_{N,
      [this, N](double t, const double x[], double y[]) -> int {
        auto xview = gsl_vector_const_view_array(x, N);
        auto yview = gsl_vector_view_array(y, N);
        gsl::vector<double> xv; xv.id = &xview.vector;
        gsl::vector<double> yv; yv.id = &yview.vector;
        func_(t, yv, xv);
        yv.id = nullptr;
        xv.id = nullptr;
        return GSL_SUCCESS;
      },
      [this, N, alg](double t, const double xa[], double *dydx, double dydt[]) -> int {
        throw "Solver "+gsl::odeiv2::step(alg, N).name()+" requires Jacobian.";
        return GSL_EBADFUNC;
      }
    },
    drv_(sys_, alg, hs, epsa, epsr),
    step_(alg, N),
    ctrl_(epsa, epsr),
    evol_(N),
    t_start_(start),
    t_end_(end)
  {
    if (req_driver(alg)) step_.set_driver(drv_);
  }

  evolution(size_t N, int alg, double start, double end,
      double epsa, double hs,
      func_t func, jac_t jac,
      double ht = 1e-3, double epsr = 0) :
    evolution(N, alg, start, end, epsa, hs, func, ht, epsr)
  {
    jac_ = jac;
    sys_.set_jacobian([this, N](double t, const double xa[], double *dydx, double dydt[])->int {
      auto xv = gsl_vector_const_view_array(xa, N);
      auto Jv = gsl_matrix_view_array(dydx, N, N);
      auto rv = gsl_vector_view_array(dydt, N);
      gsl::vector<double> x; x.id = &xv.vector;
      gsl::vector<double> r; r.id = &rv.vector;
      gsl::matrix<double> J; J.id = &Jv.matrix;

      gsl::vector<double> b(N), z(N);
      z.set_basis(0);
    	for (int i = 0; i < N; ++i)
      {
        if (i != 0) z.swap_elements(i-1,i);
        jac_(t, b, /* unused */x, z);
        J.set_col(i, b);
      }

      if (time_dependent_)
      {
        func_(t+ht_, r, x);
        func_(t-ht_, b, x);
        r -= b;
        r *= .5/ht_;
      }
      else
      {
        r.set_zero();
      }

      x.id = nullptr;
      J.id = nullptr;
      r.id = nullptr;
      return GSL_SUCCESS;
    });
  }

  int evolve(gsl::vector<double>& u,
      std::function<bool(double, size_t)> keep_going,
      stepcb_t after_step = stepcb_)
  {
    double t = t_start_;
    size_t count = 0;
    int status = GSL_SUCCESS;
    if (!keep_going) keep_going = [this](double t, size_t) {return t < t_end_;};
    while (keep_going(t, count))
    {
      const gsl::vector<double> up = u;
      status = evol_.apply(ctrl_, step_, sys_, t, t_end_, hs_, u.ptr(0));
      ++count;
      if (renormalize_ != nullptr) renormalize_(t, u);
      if (!after_step(status, t, u, up)) break;
    }
    return status;
  }

  int evolve(gsl::vector<double>& u, stepcb_t after_step = stepcb_)
  {  return evolve(u, nullptr, after_step); }

  int evolve_fixed_step(gsl::vector<double>& u,
      std::function<bool(double, size_t)> keep_going,
      stepcb_t after_step = stepcb_)
  {
    double t = t_start_;
    size_t count = 0;
    int status = GSL_SUCCESS;
    if (!keep_going) keep_going = [this](double t, size_t) {return t < t_end_;};
    while (keep_going(t, count))
    {
      const gsl::vector<double> up = u;
      double h = hs_;
      while ((status = evol_.apply_fixed_step(ctrl_, step_, sys_, t, h, u.ptr(0)))
             == GSL_FAILURE && h > 1e-300)
        h *= .5;
      ++count;
      if (renormalize_ != nullptr) renormalize_(t, u);
      if (!after_step(status, t, u, up)) break;
    }
    return status;
  }

  int evolve_fixed_step(gsl::vector<double>& u, stepcb_t after_step = stepcb_)
  {  return evolve_fixed_step(u, nullptr, after_step); }

  inline void set_time_dependent(bool td) noexcept {time_dependent_ = td;}
  inline void set_renormalization(std::function<void(double, gsl::vector<double>&)> r)
  { renormalize_ = r; }

  inline double start() const noexcept {return t_start_;}
  inline double end() const noexcept {return t_end_;}

protected:

  inline static bool req_driver(int alg) noexcept
  {
    return alg == gsl::odeiv2::RK1IMP  || alg == gsl::odeiv2::RK2IMP ||
           alg == gsl::odeiv2::RK4IMP  || alg == gsl::odeiv2::BSIMP  ||
           alg == gsl::odeiv2::MSADAMS || alg == gsl::odeiv2::MSBDF;
  }

  static bool stepcb_(int status, double, const gsl::vector<double>&,
                      const gsl::vector<double>&) {return status == GSL_SUCCESS;}
};

} // namespace odeiv2
} // namespace gsl

#endif // GSLEVOL_HPP_INCLUDED
