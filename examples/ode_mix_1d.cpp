// *** Generated by fidlab on Sat Dec  5 18:38:55 2020 ***
// *** Do not edit unless you know what you are doing! ***

#include <iostream>
#include <fstream>
#include "ode_mix_1d.hpp"
#include "gslevol.hpp"

using gsl::vector;

void PDE1d::func(double, vector<double>& F, const vector<double>& u)
{
	for (int i = 0; i < nx; ++i)
	{
		if (i == 0)
		{
			F[e(i)] = (-3*u[e(0)]+4*u[e(1)]-1*u[e(2)])/(2*hx)-bc0(X(i));
		}
		else if (i == nx-1)
		{
			F[e(i)] = u[e(i)]-bc1(X(i));
		}
		else
		{
			F[e(i)] = (u[e(i+1)]-2*u[e(i)]+u[e(i-1)])/(hx*hx)+((fa(X(i)))*((u[e(i+1)]-u[e(i-1)])/(2*hx)))+((fb(X(i)))*(u[e(i)]))-(f(X(i)));
		}
	}
	F[e(nx-1)] = 0;
	F[e(0)] = (4*F[e(1)]-F[e(2)])/(3.);
}

void PDE1d::jac(double, vector<double>& J, const vector<double>& u, const vector<double>& du)
{
	for (int i = 0; i < nx; ++i)
	{
		if (i == 0)
		{
			J[e(i)] = (-3*du[e(0)]+4*du[e(1)]-1*du[e(2)])/(2*hx);
		}
		else if (i == nx-1)
		{
			J[e(i)] = du[e(i)];
		}
		else
		{
			J[e(i)] = (du[e(i+1)]-2*du[e(i)]+du[e(i-1)])/(hx*hx)+((fa(X(i)))*((du[e(i+1)]-du[e(i-1)])/(2*hx)))+((fb(X(i)))*(du[e(i)]))-(0);
		}
	}
	J[e(nx-1)] = 0;
	J[e(0)] = (4*J[e(1)]-J[e(2)])/(3.);
}

double norm_inf(const gsl::vector<double>& v)
{
  double max = 0;
  for (size_t i = 0; i < v.size(); ++i)
    if (max < std::fabs(v[i])) max = std::abs(v[i]);
  return max;
}

using namespace gsl::odeiv2;
using namespace gsl;

using std::placeholders::_1;
using std::placeholders::_2;
using std::placeholders::_3;
using std::placeholders::_4;

int main()
{
  PDE1d pde(200);
  const int N = pde.dim();
  evolution evol(N, RK4IMP, 0, 10000, 1e-7, 1e-5,
    std::bind(&PDE1d::func, pde, _1, _2, _3),
    std::bind(&PDE1d::jac, pde, _1, _2, _3, _4)
  );
  vector<double> u(N);

  // Set initial condition; it has to satisfy BCs
  constexpr double u0 = -.2;
  for (int i = 0; i < pde.nx; ++i)
    u[pde.e(i)] = pde.X(i) < (1 - u0)/M_PI_2 ? u0 + M_PI_2*pde.X(i) : 1.;

  int rc = 0;
  size_t count;
  int status = evol.evolve(u,//_fixed_step(u,
    [&evol,&count](double t, size_t nit) {
      count = nit;
      return t < evol.end();
    },
    [&pde,&rc,&count](int status, double t, const gsl::vector<double>& u,
                                            const gsl::vector<double>& up) {
      if (status != GSL_SUCCESS)
      {
        std::cout << "error, return value " << status << "\n";
        return false; // do not continue, break
      }

      gsl::vector<double> v = u; v -= up;
      const double dp = norm_inf(v);
      const double norminf = pde.dnormsup(u);
      std::cout << t << ": " << norminf << " " << dp << "\n";
      if (count > 10 && norminf > 100)
      {
        std::cout << count << " " << "blow up\n";
        rc = 4;
        return false;
      }

      pde.func(0, v, u);
      std::cout << "|u_t|_inf " << norm_inf(v) << "\n";
      if (count > 10 && (dp < 1e-8 || norm_inf(v) < 1e-9))
      {
        int dummy;
        std::cout << "input any number to continue, -1 to end: "; std::cin >> dummy;
        if (dummy == -1) return false; // do not continue, break
      }

      return true;
    }
  );

  std::cout << "iterations: " << count << "\n";

  // Output solution
  std::ofstream of("heat.txt");
  for (int i = 0; i < pde.nx; ++i)
    of << pde.X(i)
       << " " << u[pde.e(i)]
       << " " << pde.exact_solution(pde.X(i))
       << '\n';

  std::cout << "|u-u_exact|_2 = " << pde.dnorm2(u)
            << " |u-u_exact|_max = " << pde.dnormsup(u) << '\n';

  return rc;
}
