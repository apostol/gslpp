#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include "gslevol.hpp"

struct MatElemAccessor {
  const size_t dim;
  inline size_t operator()(size_t i, size_t j) const noexcept {
    return i*dim+j;
  }
};

double norm_inf(const gsl::vector<double>& v)
{
  double max = 0;
  for (size_t i = 0; i < v.size(); ++i)
    if (max < std::fabs(v[i])) max = std::abs(v[i]);
  return max;
}

gsl::vector<double> dif(const gsl::vector<double>& a, const gsl::vector<double>& b)
{
  gsl::vector<double> r(a.size());
  for (size_t i = 0; i < a.size(); ++i) r[i] = b[i] - a[i];
  return r;
}

void save(const std::string& file, const gsl::vector<double>& u, const double h)
{
  std::ofstream of;
  of.open(file);
  if (!of) {std::cout << "could not open file " << file << "\n"; exit(2);}
  for (size_t i = 0; i < u.size(); i++)
  {
    const double x = i*h;
    of << x << " " << u[i]*h*h << " " << .5*x*(x-1) << "\n";
  }
  of.close();
}

using namespace gsl::odeiv2;

int main()
{
  static constexpr size_t N = 100;
  double h = 1./double(N-1);
  int rc = 0;

  try {

  evolution evol(N, RK8PD, 0, 10000, 1e-6, 1e-2,
    [](double, gsl::vector<double>& y, const gsl::vector<double>& x) {
      y[0] = x[0];
      y[N-1] = x[N-1];
      for (size_t i = 1; i < N-1; ++i)
        y[i] = x[i-1] - 2*x[i] + x[i+1] - 1;
    }
  );

  gsl::vector<double> u(N), w(N);

  u.set_zero();
  for (size_t i = 0; i < N; i++)
  {
    const double x = i*h;
    w[i] = .5*x*(x-1)/(h*h);
  }

  size_t count;
  int status = evol.evolve(u,
    [&evol,&count](double t, size_t nit) {count = nit; return t < evol.end();},
    [&w,&rc,h,&count](int status, double t, const gsl::vector<double>& u,
                                            const gsl::vector<double>& up)
    {
      if (status != GSL_SUCCESS)
      {
        std::cout << "error, return value " << status << "\n";
        return false; // do not continue, break
      }

      const double norminf = h*h*norm_inf(dif(u,w));
      const double dp = h*h*norm_inf(dif(u,up));
      std::cout << t << ": " << norminf << " " << dp << "\n";
      if (norminf >= 0.125)
      {
        std::cout << "blow up\n";
        save("heat.txt", up, h);
        rc = 4;
        return false;
      }

      save("heat.txt", u, h);
      if (count > 10 && dp < 1.e-8)
      {
        int dummy;
        std::cout << "input any number to continue, -1 to end: "; std::cin >> dummy;
        if (dummy == -1) return false; // do not continue, break
      }

      return true;
    }
  );

  std::cout << "iterations: " << count << "\n";

  } catch (const std::string& msg) {
    std::cout << "Error: " << msg << "\n";
    return 1;
  }

  return rc;
}
