#include <iostream>
#include "gsl.hpp"

int main()
{
  gsl::sp::matrix<double> a(5,4);
  int n;

  std::cout << "a matrix size = " << a.id->size1 << " x " << a.id->size2
            << std::endl;
  std::cout << "  nz size     = " << a.nnz() << std::endl;
  #if 0
  a.set(0, 2, 3.1);
  a.set(0, 3, 4.6);
  a.set(1, 0, 1.0);
  a.set(1, 2, 7.2);
  a.set(3, 0, 2.1);
  a.set(3, 1, 2.9);
  a.set(3, 3, 8.5);
  a.set(4, 0, 4.1);
  #else
  a(0, 2) = 3.1;
  a(0, 3) = 4.6;
  a(1, 0) = 1.0;
  a(1, 2) = 7.2;
  a(3, 0) = 2.1;
  a(3, 1) = 2.9;
  a(3, 3) = 8.5;
  a(4, 0) = 4.1;
  #endif // 0
  const gsl::sp::matrix<double>& const_a =
    const_cast<const gsl::sp::matrix<double>&>(a);
  // double &xr = const_cast<double&>(const_a(0,1));
  // xr = 999;
  double x = a(0,2);
  std::cout << "a(0,2) = " << x << std::endl;
  x = const_a(0,2);
  std::cout << "a(0,2) (CONST) = " << x << std::endl;
  std::cout << "a(0,2) (CONST) = " << const_a(0,2) << std::endl;
  std::cout << "a:" << std::endl;
  // gsl_spmatrix_fprintf(stdout, a.id, "%g");

  gsl::sp::matrix<double> b = a.ccs();
  std::cout << "b:" << std::endl;
  // gsl_spmatrix_fprintf(stdout, b.id, "%g");

  gsl_spmatrix *B = b.id;
  std::cout << "b matrix size = " << b.id->size1 << " x " << b.id->size2
            << std::endl;
  std::cout << "  nz size     = " << b.nnz() << std::endl;
  std::cout << "matrix b in compressed row format:\n";
  std::cout << "i = [";
  for (int i = 0; i < B->nz; ++i)
    std::cout << B->i[i] << (i == B->nz - 1 ? "" : ", ");
  std::cout << "]\n";

  std::cout << "p = [";
  for (int i = 0; i < B->size1 + 1; ++i)
    std::cout << B->p[i] << (i == B->size1 ? "" : ", ");
  std::cout << "]\n";

  std::cout << "d = [";
  for (int i = 0; i < B->nz; ++i)
    std::cout << B->data[i] << (i == B->nz - 1 ? "" : ", ");
  std::cout << "]\n";

  gsl::sp::matrix<double> c = a.crs();
  std::cout << "c:" << std::endl;
  gsl_spmatrix_fprintf(stdout, c.id, "%g");

  gsl_spmatrix *C = c.id;
  std::cout << "c matrix size = " << c.id->size1 << " x " << c.id->size2
            << std::endl;
  std::cout << "  nz size     = " << c.nnz() << std::endl;
  std::cout << "matrix c in compressed row format:\n";
  std::cout << "i = [";
  for (int i = 0; i < C->nz; ++i)
    std::cout << C->i[i] << (i == C->nz - 1 ? "" : ", ");
  std::cout << "]\n";

  std::cout << "p = [";
  for (int i = 0; i < C->size1 + 1; ++i)
    std::cout << C->p[i] << (i == C->size1 ? "" : ", ");
  std::cout << "]\n";

  std::cout << "d = [";
  for (int i = 0; i < C->nz; ++i)
    std::cout << C->data[i] << (i == C->nz - 1 ? "" : ", ");
  std::cout << "]\n";

  return 0;
}
