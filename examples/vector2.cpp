#include <iostream>
#include "gsl.hpp"

int main()
{
  //gsl_check_range = 0;
  gsl::vector<double> a(5);

  std::cout << "length of block = " << a.id->size << std::endl;
  a.set(1, -1.2e-3);
  a[2] = 55.1;
  double *p = a.ptr(0);
  std::cout << "a[1] = " << a.get(1) << std::endl
            << "a[2] = " << a[2] << std::endl
            << "ptr = " << p << '\n';
  a[3] = a[2] + 1;
  gsl_vector_fprintf(stdout, a.id, "%g");
  //a.set(7, 3.6);
  return 0;
}

